#!/bin/bash

lista_file="./lista.txt"

while IFS= read -r filename; do
    if [ -f "$filename" ]; then
        md5=$(md5sum "$filename" | cut -d ' ' -f 1)
        sha256=$(sha256sum "$filename" | cut -d ' ' -f 1)
        echo "$md5 $filename" >> resultado.txt
        echo "$sha256 $filename" >> resultado.txt
    else
        echo "O arquivo $filename não existe."
    fi
done < "$lista_file"

