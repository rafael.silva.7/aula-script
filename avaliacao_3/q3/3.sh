#!/bin/bash
read -p "qual será o nome do dragão? " dragon_name
player_health=$1
dragon_health=$2

while true; do
    echo "Menu:"
    echo "a) Atacar"
    echo "f) Fugir"
    echo "i) Informações"
    echo "q) Sair"

    read -p "Escolha uma opção: " option

    if [ "$option" == "a" ]; then
        echo "Você ataca o $dragon_name e causa 100 pontos de dano!"
        dragon_health=$((dragon_health - 100))
        echo "$dragon_name contra-ataca e te causa 10 pontos de dano!"
        player_health=$((player_health - 10))
    elif [ "$option" == "f" ]; then
        chance=$((RANDOM % 2))
        if [ $chance -eq 0 ]; then
            echo "Você conseguiu fugir do $dragon_name! Parabéns!"
            break
        else
            echo "Você tenta fugir, mas é atingido pela baforada do $dragon_name! Você falhou em fugir!"
            player_health=0
        fi
    elif [ "$option" == "i" ]; then
        echo "Vida restante do jogador: $player_health"
        echo "Vida restante do $dragon_name: $dragon_health"
    elif [ "$option" == "q" ]; then
        echo "Saindo do jogo..."
        exit 0
    else
        echo "Opção inválida. Por favor, escolha uma opção válida."
    fi

    if [ $player_health -le 0 ]; then
        echo "Você foi derrotado pelo $dragon_name! Game over!"
        break
    fi

    if [ $dragon_health -le 0 ]; then
        echo "Você derrotou o $dragon_name! Parabéns, você venceu!"|cowsay -n
        break
    fi

    echo
done

