#!/bin/bash

while true; do
    echo "Menu:"
    echo "a) Verificar se o usuário existe"
    echo "b) Verificar se o usuário está logado na máquina"
    echo "c) Listar os arquivos da pasta home do usuário"
    echo "d) Sair"

    read -p "Escolha uma opção: " option

    if [ "$option" == "a" ]; then
        read -p "Digite o nome de usuário: " username
        id "$username" >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "O usuário $username existe."
        else
            echo "O usuário $username não existe."
	    exit
        fi
    elif [ "$option" == "b" ]; then
        read -p "Digite o nome de usuário: " username
        who | grep -wq "$username"
        if [ $? -eq 0 ]; then
            echo "O usuário $username está logado na máquina."
        else
            echo "O usuário $username não está logado na máquina."
	    exit
        fi
    elif [ "$option" == "c" ]; then
        read -p "Digite o nome de usuário: " username
        ls -l "/home/$username"
    elif [ "$option" == "d" ]; then
        break
    else
        echo "Opção inválida. Por favor, escolha uma opção válida."
    fi

    echo
done

