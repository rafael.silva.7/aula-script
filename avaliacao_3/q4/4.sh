#!/bin/bash

player_health=$((RANDOM % 191 + 10 + 24))
dragon_health=$((RANDOM % 4901 + 100))
read -p "qual o nome do dragão? " dragon_name

while true; do
    echo "Menu:"
    echo "a) Atacar"
    echo "f) Fugir"
    echo "c) Curar"
    echo "i) Informações"
    echo "q) Sair"

    read -p "Escolha uma opção: " option

    if [ "$option" == "a" ]; then
        dano_jogador=$((RANDOM % 91 + 10))
        dragon_health=$((dragon_health - dano_jogador))
        echo "Você ataca o $dragon_name e causa $dano_jogador pontos de dano!"

        if [ $dragon_health -le 0 ]; then
            echo "Você derrotou o $dragon_name! Parabéns, você venceu!"
            break
        fi

        ataques=$((RANDOM % 5 + 1))
        dano_dragao=0
        i=1
        while [ $i -le $ataques ]; do
            dano=$((RANDOM % 10 + 1))
            dano_dragao=$((dano_dragao + dano))
            i=$((i + 1))
        done
        player_health=$((player_health - dano_dragao))
        echo "$dragon_name contra-ataca e te causa $dano_dragao pontos de dano!"

        if [ $player_health -le 0 ]; then
            echo "Você foi derrotado pelo $dragon_name! Game over!"
            break
        fi
    elif [ "$option" == "f" ]; then
        chance=$((RANDOM % 5))
        if [ $chance -eq 0 ]; then
            echo "Você conseguiu fugir do $dragon_name! Parabéns!"
            break
        else
            echo "Você tenta fugir, mas é atingido pela baforada do $dragon_name! Você falhou em fugir!"
            player_health=0
            echo "Você foi derrotado pelo $dragon_name! Game over!"
            break
        fi
    elif [ "$option" == "c" ]; then
        cura=$((RANDOM % 51 + 50))
        player_health=$((player_health + cura))
        echo "Você se cura e recupera $cura pontos de vida!"

        chance=$((RANDOM % 10))
        if [ $chance -eq 0 ]; then
            echo "$dragon_name tenta fugir, mas você o impede!"
        fi
    elif [ "$option" == "i" ]; then
        echo "Vida restante do jogador: $player_health"
        echo "Vida restante do $dragon_name: $dragon_health"
    elif [ "$option" == "q" ]; then
        echo "Saindo do jogo..."
        exit 0
    else
        echo "Opção inválida. Por favor, escolha uma opção válida."
    fi

    echo
done

