#!/bin/bash
mkdir ~/$1
mkdir ~/$2
mkdir ~/$3
mkdir ~/$4

touch ~/$1/README.md
touch ~/$2/README.md
touch ~/$3/README.md
touch ~/$4/README.md

echo $1 >> ~/$1/README.md
echo $2 >> ~/$2/README.md
echo $3 >> ~/$3/README.md
echo $4 >> ~/$4/README.md
