#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Uso: $0 <N>"
    exit 1
fi

N=$1

if [ ! -f "A.txt" ] || [ ! -f "B.txt" ] || [ ! -f "C.txt" ] || [ ! -f "D.txt" ]; then
    echo "Arquivos necessários não encontrados."
    exit 1
fi

for ((i=1; i<=N; i++)); do
    senha=$(cat A.txt | shuf -n1 | tr -d '\n')
    senha+=$(cat B.txt | shuf -n1 | tr -d '\n')
    senha+=$(cat C.txt | shuf -n1 | tr -d '\n')
    senha+=$(cat D.txt | shuf -n1 | tr -d '\n')
    senha=$(echo "$senha" | fold -w1 | shuf | tr -d '\n')
    echo "$senha"
done

