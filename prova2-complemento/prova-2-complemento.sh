#!/bin/bash

arq1=$1 
arq2=$2 
arq3=$3 
arq4=$4 

linhas1=$(wc -l < "$arq1" 2>/dev/null) 
linhas2=$(wc -l < "$arq2" 2>/dev/null)  
linhas3=$(wc -l < "$arq3" 2>/dev/null) 
linhas4=$(wc -l < "$arq4" 2>/dev/null)
                          
max_linha=$linhas1        
max_arq=$arq1

[ $linhas2 -gt $max_linha ] 2>/dev/null && max_arq=$arq2
[ $linhas3 -gt $max_linha ] 2>/dev/null && max_arq=$arq3
[ $linhas4 -gt $max_linha ] 2>/dev/null && max_arq=$arq4
cat < $max_arq
