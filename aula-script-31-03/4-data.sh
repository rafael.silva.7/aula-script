#!/bin/bash
d=$(date +%Y-%m-%d-%H)
r=/tmp/$d
mkdir $r
cp * $r

tar -czf aula.tar.gz $r
rm -rf $r
