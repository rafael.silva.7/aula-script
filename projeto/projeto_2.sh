#!/bin/bash

# Arquivo para salvar IPs/hosts
arquivo_ips="ips.txt"

# Verificar se o servidor SSH está instalado
dpkg -s ssh &> /dev/null
if [[ $? -eq 0 ]]; then
    yad --title="SSH Server" --text="O servidor SSH está instalado." --button="OK:0" --width=300 --height=100
else
    # Solicitar instalação do servidor SSH
    yad --title="SSH Server" --text="O servidor SSH não está instalado. Deseja instalá-lo?" --button="Sim:0" --button="Não:1" --width=400 --height=100
    response=$?
    if [[ $response -eq 0 ]]; then
        # Instalar o servidor SSH
        sudo apt update
        sudo apt install ssh -y
        sudo systemctl enable ssh
        sudo systemctl start ssh
        yad --title="SSH Server" --text="O servidor SSH foi instalado e iniciado com sucesso." --button="OK:0" --width=300 --height=100
    else
        yad --title="SSH Server" --text="O servidor SSH não foi instalado." --button="OK:0" --width=300 --height=100
    fi
fi

# Solicita
login=""
while [[ -z "$login" ]]; do
    login=$(yad --title="Login" --text="Digite seu nome de usuário:" --entry --width=300 --height=100)
done

senha=""
while [[ -z "$senha" ]]; do
    senha=$(yad --title="Senha" --text="Digite sua senha:" --entry --hide-text --width=300 --height=100)
done

# Exibi informações de login
yad --title="Login" --text="Usuário: $login\nSenha: $senha" --button="OK:0" --width=300 --height=100

# Seleciona arquivo local
arquivo=$(yad --title="Selecione um arquivo" --file --width=500 --height=300)

# Solicita informações do host de destino
host_destino=""
while [[ -z "$host_destino" ]]; do
    host_destino=$(yad --title="Host de destino" --text="Digite o endereço do host de destino:" --entry --width=300 --height=100)
done

usuario_destino=""
while [[ -z "$usuario_destino" ]]; do
    usuario_destino=$(yad --title="Usuário de destino" --text="Digite o nome de usuário do host de destino:" --entry --width=300 --height=100)
done

diretorio_destino=""
while [[ -z "$diretorio_destino" ]]; do
    diretorio_destino=$(yad --title="Diretório de destino" --text="Digite o diretório de destino no host de destino:" --entry --width=300 --height=100)
done

# Salvar IP/host para uso futuro
echo "$host_destino $usuario_destino" >> "$arquivo_ips"

# Transferi arquivo usando scp
scp "$arquivo" "$usuario_destino@$host_destino:$diretorio_destino"
if [[ $? -eq 0 ]]; then
    # Verificar md5 dos arquivos
    md5_original=$(md5sum "$arquivo" | awk '{ print $1 }')
    md5_destino=$(ssh "$usuario_destino@$host_destino" "md5sum '$diretorio_destino/$(basename "$arquivo")'" | awk '{ print $1 }')

    if [[ "$md5_original" == "$md5_destino" ]]; then
        yad --title="Transferência bem-sucedida" --text="A cópia do arquivo foi transferida com sucesso (MD5: $md5_destino)." --button="OK:0" --width=400 --height=100
    else
        exibir_erro "Falha na transferência" "Ocorreu um erro ao transferir o arquivo. A verificação MD5 falhou."
    fi
else
    exibir_erro "Falha na transferência" "Ocorreu um erro ao transferir o arquivo."
fi


# Exibi lista de arquivos remotos
lista_arquivos=$(ssh "$usuario_destino@$host_destino" "ls -p $diretorio_destino | grep -v /")
yad --title="Lista de Arquivos Remotos" --text="Arquivos no diretório remoto $diretorio_destino:\n$lista_arquivos" --button="OK:0" --width=500 --height=300

