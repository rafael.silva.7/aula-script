#~/.bashrc

#prompt do PS1
#cores
endc='\[\033[00m\]'
bwhite='\[\033[01;37;40m\]'
bcyan='\[\033[01;36m\]'
bgreen='\[\033[01;32m\]'
bpink='\[\033[01;35m\]'
bred='\[\033[01;31m\]'
yellow='\[\033[00;33m\]'

#usuário comum
c0="$bgreen"

#checar código de saída da última linha de comando
prompt_exit="\$( ((e=\$? , e)) && echo \"${bred}\${e}${endc} \" )"

#número de componentes de diretório para reter em \w e \W
PROMPT_DIRTRIM=3

#setar a string do ps1
PS1="${prompt_exit}${c0}\u${bwhite} \h:${bcyan}\w${endc} ${prompt_git}(\!)${prompt_ssl}\$ "

#manter o ambiente limpo
unset end bwhite bcyan bgreen bpink bred yellow c0 prompt_ssl_max prompt_ssl prompt_git prompt_exit 
