#!/bin/bash

arquivo_sites="lista_de_sites.txt"
diretorio_destino="downloads"
if [ ! -d "$diretorio_destino" ]; then
    mkdir "$diretorio_destino"
fi
for site in $(cat "$arquivo_sites"); do
    nome_diretorio="$diretorio_destino/$site"

    if [ ! -d "$nome_diretorio" ]; then
        mkdir "$nome_diretorio"
    fi
    wget -P "$nome_diretorio" "http://$site"
    echo "Download do site $site concluído."
done

