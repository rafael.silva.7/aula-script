#!/bin/bash

echo "Verificando diretório /etc:"
for arq_cam in /etc/*; do
    if [ -d "$arq_cam" ]; then
        echo "$arq_cam: é um Diretório"
    elif [ -f "$arq_cam" ]; then
        if [ -x "$arq_cam" ]; then
            echo "$arq_cam: é um Executável"
        else
            echo "$arq_cam: é um Arquivo"
        fi
    elif [ -L "$arq_cam" ]; then
        echo "$arq_cam: é um Link simbólico"
    else
        echo "$arq_cam: Desconhecido!!!"
    fi
done

echo "Verificando diretório /tmp:"
for arq_cam in /tmp/*; do
    if [ -d "$arq_cam" ]; then
        echo "$arq_cam: é um Diretório"
    elif [ -f "$arq_cam" ]; then
        if [ -x "$arq_cam" ]; then
            echo "$arq_cam: é um Executável"
        else
            echo "$arq_cam: é um Arquivo"
        fi
    elif [ -L "$arq_cam" ]; then
        echo "$arq_cam: é um Link simbólico"
    else
        echo "$arq_cam: Desconhecido"
    fi
done

