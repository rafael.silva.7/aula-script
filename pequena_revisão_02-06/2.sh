#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Por favor, forneça um número como argumento."
    exit 1
fi

num=$1
soma=0

for ((i=1; i<num; i++)); do
    soma=$((soma + i))
done

echo "A soma de todos os números de 1 até $num é: $soma"

