#!/bin/bash

while true; do
    clear
    echo "Menu:"
    echo "a -> Executar ping para um host (IP ou site)"
    echo "b -> Listar os usuários atualmente logados na máquina"
    echo "c -> Exibir o uso de memória e de disco da máquina"
    echo "d -> Sair"

    read -p "Escolha uma opção: " opcao

    if [ "$opcao" = "a" ]; then
        read -p "Digite o IP ou site para executar o ping: " host
        ping -c 4 "$host"
    elif [ "$opcao" = "b" ]; then
        who
    elif [ "$opcao" = "c" ]; then
        free -h
        df -h
    elif [ "$opcao" = "d" ]; then
        break
    else
        echo "Opção inválida. Tente novamente."
    fi

    read -p "Pressione Enter para continuar..."
done

echo "Encerrando o programa..."

